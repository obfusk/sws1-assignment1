#!/bin/bash

set -e

choices=( A C G T )
lines=500
chars=100

for (( i = 0 ; i < lines ; ++i )); do
  for (( j = 0 ; j < chars ; ++j )); do
    n=$(( RANDOM % ${#choices[@]} ))
    c="${choices[n]}"
    echo -n "$c"
  done
  echo
done

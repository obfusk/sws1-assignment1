#include <stdio.h>

#define CHOICES   "ACGT"
#define CHOICES_N      4
#define LINES        500
#define CHARS        100

const char *choices = CHOICES;

/*
  parses A/C/T/G; updates distrib; returns 1 if parse succeeds, 0
  otherwise
*/
int parse_choice(const char c, size_t distrib[]) {
  for (size_t i = 0; i < CHOICES_N; ++i) {
    if (c == choices[i]) { ++distrib[i]; return 1; }
  }
  return 0;
}

#define OOPS_EOF    1
#define OOPS_NOACGT 2
#define OOPS_NOEOL  3
#define OOPS_NOEOF  4

/*
  parses file; updates distrib; returns 0 if parse succeeds, one of
  the OOPS_* otherwise
*/
int parse(FILE *f, size_t distrib[]) {
  int count = 0, c;
  for (size_t i = 0; i < LINES; ++i) {
    for (size_t j = 0; j < CHARS; ++j) {
      if ((c = getc(f)) == EOF) return OOPS_EOF;
      if (!parse_choice(c, distrib)) return OOPS_NOACGT;
      ++count;
    }
    if ((c = getc(f)) != '\n') return OOPS_NOEOL;
  }
  if ((c = getc(f)) != EOF) return OOPS_NOEOF;
  return 0;
}

/*
  reads from stdin or first arg; parses; prints distrib
*/
int main(int argc, char *argv[]) {
  size_t distrib[CHOICES_N];
  for (size_t i = 0; i < CHOICES_N; ++i) { distrib[i] = 0; }

  FILE *f; int close = 0;

  if (argc == 1) {
    f = stdin;
  } else if (argc == 2) {
    if (!(f = fopen(argv[1], "r"))) return 2;
    close = 1;
  } else {
    return 3;
  }

  int p = parse(f, distrib);
  if (close) { fclose(f); }
  if (p != 0) return -1;

  for (size_t i = 0; i < CHOICES_N; ++i) {
    printf("%c: %zu\n", choices[i], distrib[i]);
  }

  return 0;
}

#!/bin/bash
if [ "$#" = 1 ]; then
  file=/dev/stdin string="$1"
elif [ "$#" = 2 ]; then
  file="$1" string="$2"
else
  exit 1
fi
tr -d '\n' < "$file" | grep -o "$string" | wc -l
